def clone2() {
  git branch: 'branch2', url: 'https://gitlab.com/shipal02/mongodb-tool.git'
}

def clone1() {
  sh 'rm modules -rf; mkdir modules'
  dir ('modules') {
    git branch: 'branch1', url: 'https://gitlab.com/shipal02/mongodb-tool.git'
     }
}

def init() {
  sh 'terraform init'
  
  
}

def plan() {
  sh 'terraform plan -lock=false'
  
  
}

def apply() {
  sh 'terraform apply -auto-approve -lock=false'
}

def destroy() {
  sh 'terraform destroy -auto-approve -lock=false
}
